import React from 'react';
import './App.css';
import { useFormik } from 'formik';


const validate = values => {
  const errors = {};
  if (!values.Name) {
    errors.Name = 'Required';
  } else if (values.Name.length < 5) {
    errors.Name = 'Name must more than 5 characters';
  }
  if (!values.email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }
  if (!values.password) {
    errors.password = 'Required';
  } else if (values.password.length < 6 ) {
    errors.password = 'Password must more than 6 characters';
  }
  if (values.password !== values.confirm) {
    errors.confirm = '';
  }
  return errors;
};

const SignupForm = () => {
  // Notice that we have to initialize ALL of fields with values. These
  // could come from props, but since we don't want to prefill this form,
  // we just use an empty string. If you don't do this, React will yell
  // at you.
  const formik = useFormik({
    initialValues: {
      Name: '',
      email: '',
      password: '',
      confirm: '',
    },
    validate,
    onSubmit: values => {
      alert('Anda Berhasil SignUp');
    },
  });
  return (
  <div className="App">
    <div className='container'>
      <h1>Register</h1>
      <hr />
        <form onSubmit={formik.handleSubmit}>
          <div class="form-group">
            <label htmlFor="Name">Name</label>
              <input
              id="Name"
              name="Name"
              type="text"
              className="form-control container col-md-6"
              onChange={formik.handleChange}
              value={formik.values.Name}
              />
            {formik.errors.Name ? <div className="text-danger">{formik.errors.Name}</div> : null}
          </div>
          <div class="form-group">
            <label htmlFor="email">Email Address</label>
              <input
              id="email"
              name="email"
              type="email"
              className="form-control container col-md-6"
              onChange={formik.handleChange}
              value={formik.values.email}
              />
            {formik.errors.email ? <div className="text-danger">{formik.errors.email}</div> : null}
          </div>
          <div class="form-group">
            <label htmlFor="password">Password</label>
              <input
              id="password"
              name="password"
              type="password"
              className="form-control container col-md-6"
              onChange={formik.handleChange}
              value={formik.values.password}
              />
            {formik.errors.password ? <div className="text-danger">{formik.errors.password}</div> : null}
          </div> 
          <div class="form-group">
            <label htmlFor="confirm">Confirm</label>
              <input
              id="confirm"
              name="confirm"
              type="password"
              className="form-control container col-md-6"
              onChange={formik.handleChange}
              value={formik.values.confirm}
              />
            {formik.errors.confirm ? <div className="text-danger">{formik.errors.confirm}</div> : null}
          </div>
          <button type="submit" className="btn btn-primary">Submit</button>
        </form>
    </div>
  </div> 
  );
};

export default SignupForm