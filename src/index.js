import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Route, Link, BrowserRouter as Router, Switch } from "react-router-dom";
import About from "./About";
import Profile from "./Profile";
import Form from "./Form";
import MultiForm from "./MultiForm";
import NotFound from './NotFound';
import SignupForm from './Challenge4';
import Login from './Login';
import "bootstrap/dist/css/bootstrap.min.css";

const routing = (
  <Router>
    <nav className ="navbar navbar-expand navbar-dark bg-dark">
     
        <ul className="navbar-nav mr-auto">
          <li className="nav-item active">
            <Link className="navbar-brand" to="/">
              Home
            </Link>
          </li>
          <li className="nav-item">
            <Link className="navbar-brand" to="/about">
              About
            </Link>
          </li>
          <li className="nav-item">
            <Link className="navbar-brand" to="/profile">
              Profile
            </Link>
          </li>
          <li className="nav-item">
            <Link className="navbar-brand" to="/form">
              Form
            </Link>
          </li>
          <li className="nav-item">
            <Link className="navbar-brand" to="/multiform">
              MultiForm
            </Link>
          </li>
          <li className="nav-item">
            <Link className="navbar-brand" to="/challenge4">
              Challenge4
            </Link>
          </li>
          <li className="nav-item">
            <Link className="navbar-brand" to="/login">
              LogIn
            </Link>
          </li>
        </ul>
      
    </nav>
    <Switch>
      <Route exact path="/" component={App} />
      <Route path="/about/:number?" component={About} />
      <Route path="/profile" component={Profile} />
      <Route path="/form" component={Form} />
      <Route path="/multiform" component={MultiForm} />
      <Route path="/challenge4" component={SignupForm} />
      <Route path="/login" component={Login} />
      <Route component={NotFound} />
    </Switch>
  </Router>
)

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
