 // contoh dua
 import React from 'react';

 class MultiInputForm extends React.Component {
   constructor(props) {
     super(props);
     this.state = {
       Name: '',
       UserName: '',
       isGoing: true
     };
 
     this.handleInputChange = this.handleInputChange.bind(this);
     this.handleSubmit = this.handleSubmit.bind(this);
   }
 
   handleInputChange(event) {
     const target = event.target;
     const value = target.type === 'checkbox' ? target.checked : target.value;
     const name = target.name;
 
     this.setState({
       [name]: value
     });
   }
 
   handleSubmit(event) {
     alert('Name: ' + this.state.Name + ' \nUserName: ' + this.state.UserName +  ' \nIs going: ' + this.state.isGoing);
     event.preventDefault();
   }
 
   render() {
     return (
       <form onSubmit={this.handleSubmit}>
         <div className="form-group">
         <label>
           Name:
             <input
             name="Name"
             type="text"
             className="form-control"
             checked={this.state.Name}
             onChange={this.handleInputChange} />
         </label>
         </div>
         
         <div className="form-group">
         <label>
           UserName:
             <input
             name="UserName"
             type="text"
             className="form-control"
             value={this.state.UserName}
             onChange={this.handleInputChange} />
         </label>
        </div>
        
         <div className="form-check">
         <label className="form-check-label">
            Is going:
            <input
              name="isGoing"
              type="checkbox"
              className="form-check-input"
              checked={this.state.isGoing}
              onChange={this.handleInputChange} />
          </label>
         </div>
          
         <input type="submit" value="Submit" />
       </form>
     );
   }
 }
 
 export default MultiInputForm