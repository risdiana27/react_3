import React from 'react';

const Bio = props => (
    <p>{props.bio}</p>
);

export default Bio;
