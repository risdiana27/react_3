import React from 'react';
import Avatar from './Avatar';
import UserName from './UserName';
import Bio from './Bio';
import './UserProfile.css';


const UserProfile = props => {
  return props.name.map((name, index) => (
    <div className="container">
      <div className="card">
        <Avatar />
        <div class="card-body bg-primary">
        <UserName key = {index} name = {name} />
        <Bio bio="Plus Ultra!" />
        </div>
            
        </div>
    </div>
    
  )
   
   
);

};
  


export default UserProfile;
