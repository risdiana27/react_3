import React from 'react';

const UserName = props => (
    <p>{props.name}</p>
);

UserName.defaultProps = {
    username: "User"
}

export default UserName;
