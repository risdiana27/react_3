// Render Prop
import React from 'react';
import './App.css';
import { Formik, Form, Field, ErrorMessage } from 'formik';


const Login = () => (
  <div>
    <Formik
      initialValues={{ email: '', password: '' }}
      validate={values => {
        const errors = {};
        if (!values.email) {
          errors.email = 'Required';
        } else if (
          !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
          errors.email = 'Invalid email address';
        }
        if (!values.password) {
          errors.password = 'Required';
        }
        return errors;
      }}
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {
          alert(JSON.stringify(values, null, 2));
          setSubmitting(false);
        }, 400);
      }}
    >
      {({ isSubmitting }) => (
        <div className="App">
            <div className="container">
              <h1>Log In</h1>
              <hr />
              <Form>
                <div class="form-group container col-md-6">
                  <label htmlFor="email">Email</label>
                  <Field type="email" name="email" className="form-control"/>
                  <ErrorMessage name="email" component="div" />
                </div>
                
                <div class="form-group container col-md-6">
                  <label htmlFor="paswword">Password</label>
                  <Field type="password" name="password" className="form-control" />
                  <ErrorMessage name="password" component="div" />  
                </div>
                
                <button type="submit"  className="btn btn-primary" disabled={isSubmitting}>
                  Submit
                </button>
              </Form>
              </div>
          </div>
      )}
    </Formik>
  </div>
);

export default Login;
